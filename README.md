The Hours of Jeanne D'Evreux
A Prayer Book for A Queen

Narrated by Phillpee de Montebello
______________________________________


This is a Macromedia Director disk that requires Quicktime to play. Versions tested include Quicktime 3 and 4, which are included.

The CD was found at Goodwill and was about to become landfill trash. Therefore, it is assumed whatever copyrights are applicable to this media are null and void since the entire work would have been lost without some kind of intervention. Since the cd contains images of a 14th century prayer book to a person that held the title of Royalty, it would have been a shame to see it thrown away like trash.

The CD has audio which should play. I have included cue/bin files as well as ISO's. The suggested way to play the cd is to use a distribution like Linux and download wine. Then, install quicktime via wine, and run the following command after mounting or extracting the disk.

wine devreux.exe


Note: With wine, the included iso seemed to play as expected moreso than the cue/bin. Probably best to convert cue/bin to iso if you wish to use it for some reason.

Further note: Attempted to use conty to have a portable wine with this release for ease of use, but proprietary graphic cards like Nvidia impeded the ability to play the CD reliably. Hopefully, efforts to "reverse engineer" Macromedia Director / Shockwave projects into something like twine websites can happen so we do not lose other valuable publications.


# Hash info for File Verification:


## Hours.bin

 MD5
9c9fb0747dbe01c3f20be2604e17ad6b  hours.bin
 SHA1
b140b0069ada66d496c7d81eaf330d07227b9747  hours.bin
 SHA256
58314bc73c434e1eb819fa30ff8258c65cb4b01c371dad1e7ed25951a50d09a8  hours.bin
 CRC32
aa10f090  hours.bin


## Hours.cue

 MD5
b940ebc97a216910e47e6ba854a9a2f6  hours.cue
 SHA1
4c1166e51192b836e9e2972ba0963faadea1f7bb  hours.cue
 SHA256
c5d2968e37883eaf73aba61305ee56228c73eaab640eea342af16706e8dd5eb0  hours.cue
 CRC32
4e3bec9a  hours.cue

## Jeanne D'Evreux iso

 MD5
6a5953963c4a43ed0a869886952fea99  jeanne_devreux.iso
 SHA1
7aea94ddacbf0c72da8d5c5fa28010571aa4ff8e  jeanne_devreux.iso
 SHA256
f60c7e2c6082b7ffa6196c121877969d85851b766a82ca36b282dc651fda69d4  jeanne_devreux.iso
 CRC32
c5ecfa8c  jeanne_devreux.iso


## Quicktime 302

 MD5
05823665fa1d89f5a0a725f614df0fe1  quicktime302.exe
 SHA1
139a3fe735e4a3fa6f37f4452d43f73c8ca0ef31  quicktime302.exe
 SHA256
35208c93ad3ef7ca33b33dc2dae03522d55248d551e30d59df1b49d4f578834f  quicktime302.exe
 CRC32
97359b6b  quicktime302.exe


## Quicktime 40

 MD5
d45bfee31cf68ed0dc3775f6e95ea689  quicktime40.exe
 SHA1
e7c3c17a864ffc7dfd06fca31d64624b24df60c9  quicktime40.exe
 SHA256
d6771f45f6a80817682f13bc6ca59a21586700e02f60e59bd09d1770e4f69050  quicktime40.exe
 CRC32
0e419bd8  quicktime40.exe
